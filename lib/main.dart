import 'package:flutter/material.dart';
import 'package:laiterie/HomeScreen.dart';

void main() {
  runApp(MyApp());
}
// void start() async {

// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Restaurant',
      theme: ThemeData(
        primaryColor: Colors.green[600],
      ),
      home: HomeScreen(),
    );
  }
}
