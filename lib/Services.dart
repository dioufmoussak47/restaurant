import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'model/restaurant.dart';

// class Services {
// ignore: missing_return
class Services {
  // String term;
  // Services({this.term});
  static final String url = "https://mlaiterie.herokuapp.com/restaurants";

  static Future<List<Restaurant>> getData() async {
    try {
      Response response = await http.get(Uri.parse(url));
      var jsonData = jsonDecode(response.body);
      List<Restaurant> restaurant = [];
      for (var item in jsonData) {
        Restaurant res = Restaurant(
            name: item["name"],
            score: item["score"],
            grade: item["grade"],
            id: item["id"],
            building: item["building"],
            street: item["street"],
            borough: item["borough"],
            cuisine: item["cuisine"]);
        restaurant.add(res);
      }

      return restaurant;
    } catch (err) {
      print("une  erreur s'est produite");
      print(err);
      return err;
    }
  }

  static Future<List<Restaurant>> searchData(String term) async {
    final String urlSearch = term.length != 0
        ? "https://mlaiterie.herokuapp.com/search?term=$term"
        : "https://mlaiterie.herokuapp.com/restaurants";

    try {
      Response response = await http.get(Uri.parse(urlSearch));
      var jsonData = jsonDecode(response.body);
      print(jsonData);
      List<Restaurant> restaurant = [];
      for (var item in jsonData) {
        Restaurant res = Restaurant(
            name: item["name"],
            score: item["score"],
            grade: item["grade"],
            id: item["id"],
            building: item["building"],
            street: item["street"],
            borough: item["borough"],
            cuisine: item["cuisine"]);
        restaurant.add(res);
      }

      return restaurant;
    } catch (err) {
      print("une  erreur s'est produite");
      print(err);
      return err;
    }
  }
}
