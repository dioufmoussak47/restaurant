import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:laiterie/Services.dart';

import 'package:laiterie/model/restaurant.dart';
// import 'package:laiterie/data.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Restaurant> _restaurants;
  bool _loading;
  final myController = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loading = true;

    Services.getData().then((restaurants) {
      this.setState(() {
        _restaurants = restaurants;
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("wait");
    // print(_restaurants);
    // print("wait");
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image(
              image: AssetImage('assets/images/logo.jpeg'),
              fit: BoxFit.contain,
            ),
          ),
        ),
        centerTitle: true,
        title: Text("Laiterie du Berger",
            style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: myController,
                      decoration: InputDecoration(
                        labelText: 'Chercher un restaurant',
                        labelStyle: TextStyle(
                            color: Colors.green,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w600),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print(myController.text);
                      Services.searchData(myController.text)
                          .then((restaurants) {
                        if (restaurants.length > 0) {
                          this.setState(() {
                            _restaurants = restaurants;
                          });
                        }
                      });
                    },
                    child: Icon(
                      Icons.search,
                      color: Colors.green,
                      size: 30.0,
                      semanticLabel: 'Text to announce in accessibility modes',
                    ),
                  ),
                ],
              ),
            ),
            // FutureBuilder(
            //   future: Services.getData(),
            //   builder: (context, snapshot) {
            //     if (snapshot.data == null) {
            //       return Container(
            //         child: Center(
            //           child: Text("There is no data loading ....."),
            //         ),
            //       );
            //     } else {
            //       return ListView.builder(
            //           primary: false,
            //           shrinkWrap: true,
            //           itemCount: snapshot.data.length,
            //           itemBuilder: (context, index) {
            //             return Container(
            //                 margin: EdgeInsets.fromLTRB(20, 15, 15, 20),
            //                 width: 200,
            //                 height: 100,
            //                 decoration: BoxDecoration(
            //                   color: Colors.green,
            //                 ),
            //                 child: Column(
            //                   children: [
            //                     Text(snapshot.data[index].name),
            //                     Text(snapshot.data[index].street),
            //                     Text(snapshot.data[index].cuisine),
            //                     Text(snapshot.data[index].borough),
            //                     Text(snapshot.data[index].building),
            //                   ],
            //                 ));
            //           });
            //     }
            //   },
            // )
            _restaurants == null
                ? Container(
                    child: Text("Loading ..."),
                  )
                : ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: _restaurants.length,
                    itemBuilder: (context, index) {
                      var restaurant = _restaurants[index];
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          shadowColor: Colors.green,
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Colors.greenAccent, Colors.green],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter),
                            ),
                            padding: EdgeInsets.all(16.0),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Nom : ${restaurant.name}",
                                    style: TextStyle(
                                        color: Colors.yellowAccent,
                                        letterSpacing: 1,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  SizedBox(
                                    height: 12.0,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Building : ${restaurant.building}",
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              fontSize: 16,
                                              letterSpacing: 1,
                                              fontWeight: FontWeight.bold)),
                                      Text("Street : ${restaurant.street}",
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              fontSize: 16,
                                              letterSpacing: 1,
                                              fontWeight: FontWeight.bold)),
                                      Text("Borough : ${restaurant.borough}",
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              letterSpacing: 1,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 12.0,
                                  ),
                                  Text("Cuisine : ${restaurant.cuisine}",
                                      style: TextStyle(
                                          color: Colors.yellow,
                                          letterSpacing: 1,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 12.0,
                                  ),
                                  Column(
                                    children: [
                                      Text("Grade : ${restaurant.grade}",
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              letterSpacing: 1,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text(
                                          "Score : ${restaurant.score.toString()}",
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              letterSpacing: 1,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
            _restaurants != null && _restaurants.length < 2
                ? OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      shape: StadiumBorder(),
                      side: BorderSide(width: 2, color: Colors.greenAccent),
                    ),
                    onPressed: () {
                      Services.getData().then((restaurants) {
                        this.setState(() {
                          _restaurants = restaurants;
                        });
                      });
                    },
                    child: Text(
                      'Voir tous les 100 restaurants',
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                : Text("fin de page"),
          ],
        ),
      ),
    );
  }
}

// GestureDetector(
//                     onTap: () {
//                       Services.getData().then((restaurants) {
//                         this.setState(() {
//                           _restaurants = restaurants;
//                         });
//                       });
//                     },
//                     child: Text("voir tous les restaurants"))
