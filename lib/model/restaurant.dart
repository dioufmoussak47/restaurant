// To parse this JSON data, do
//
//     final restaurants = restaurantsFromJson(jsonString);

import 'dart:convert';

class Restaurant {
  String name;
  int score;
  String grade;
  String id;
  String building;
  String street;
  String borough;
  String cuisine;
  Restaurant({
    this.name,
    this.score,
    this.grade,
    this.id,
    this.building,
    this.street,
    this.borough,
    this.cuisine,
  });

  // factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
  //     name: json["name"],
  //     score: json["score"],
  //     grade: json["grade"],
  //     id: json["id"],
  //     building: json["building"],
  //     street: json["street"],
  //     borough: json["borough"],
  //     cuisine: json["cuisine"]);
}
